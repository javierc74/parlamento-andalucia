<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key=" lang.dir" />" lang="${w3c_language_id}">

<head>
  <title>${the_title} - ${company_name}</title>

  <meta content="initial-scale=1.0, width=device-width" name="viewport" />

  <@liferay_util["include"] page=top_head_include />
  <!-- Fuentes Tipográficas-->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css?family=Neuton:300,400,400i,700,800&display=swap" rel="stylesheet" />

<link rel="stylesheet" type="text/css" href="${css_folder}/cookieconsent.css" />

</head>

<body class="${css_class}">

  <@liferay_ui["quick-access"] contentId="#main-content" />

  <@liferay_util["include"] page=body_top_include />

  <@liferay.control_menu />

  <div class="container" id="wrapper">

      <header class="d-none d-md-block">
          <div class="pa-header-t2">
            <div class="pa-header-row1 bg-secondary"></div>
            <div class="pa-header-row2">
              <nav class="navbar navbar-light bg-light navbar-expand justify-content-between">
                <div class="d-flex">
                  <a class="navbar-brand w-subbrand" href="http://www.parlamentodeandalucia.es/">
                    <img src="${images_folder}/logo-parlamento@2x.png" alt="Logo Parlamento de Andalucía">
                    </a>
                    <a class="subbbrand mt-3" href="${site_default_url}">
                      <img src="${images_folder}/logo-transparencia.svg" alt="Portal de Transparencia">
                      </a>
                    </div>
                    <div class="d-flex">
                      <ul class="navbar-nav">
                        <li class="nav-item">
                          <a class="nav-link" href="https://www.facebook.com/parlamentodeandalucia.es">
                            <i class="fab fa-facebook"></i>
                            <span class="sr-only">Facebook</span>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="https://twitter.com/parlamentoand">
                            <i class="fab fa-twitter-square"></i>
                            <span class="sr-only">Twitter</span>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#http://www.youtube.com/parlamentoandalucia">
                            <i class="fab fa-youtube"></i>
                            <span class="sr-only">Youtube</span>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="https://www.instagram.com/ParlamentoAnd">
                            <i class="fab fa-instagram"></i>
                            <span class="sr-only">Instagram</span>
                          </a>
                        </li>
                       
                      </ul>
                      
                     
                      <ul class="navbar-nav">
                        <li class="nav-item">
                          <a class="nav-link collapsed btn" data-toggle="collapse" href="#pa-search" role="button" aria-expanded="false" aria-controls="collapseExample">
                            <i class="fas fa-search"></i>
                            <span class="sr-only">Buscar</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </nav>
                </div>
                <div class="collapse p-2 bg-gray" id="pa-search">                   
                 <@liferay.search  />                                                                
                  </div>
                  <div class="pa-header-row3">
                    <#if has_navigation && is_setup_complete>
                      <#include "${full_templates_path}/navigation.ftl" />
                    </#if>
                  </div>
                </div>
              </header>
              <header class="d-block d-md-none">
                  <div class="pa-header-movil">
                      <nav class="navbar  navbar-light bg-light">
                          <div class="d-flex justify-content-between bg-primary navbar-dark w-100">
                              <a class="navbar-brand py-1" href="#">
                                   <img src="${images_folder}/logo-subportal-movil.svg" alt="Logo Parlamento de Andalucía">
                              </a>
                              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#pa-subMenuMovil-2" aria-controls="pa-subMenuMovil-2" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                          </div>
                          <div class="collapse navbar-collapse" id="pa-subMenuMovil-2">
                              <div class="p-2 bg-light" id="pa-search-movil">
                                   <@liferay.search  /> 
                              </div>
                              <#if has_navigation && is_setup_complete>
                      <#include "${full_templates_path}/navigation_movil.ftl" />
                    </#if>
                              <hr>
                              <div class="d-flex navbar-expand">
                                 <ul class="navbar-nav">
                        <li class="nav-item">
                          <a class="nav-link" href="https://www.facebook.com/parlamentodeandalucia.es">
                            <i class="fab fa-facebook"></i>
                            <span class="sr-only">Facebook</span>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="https://twitter.com/parlamentoand">
                            <i class="fab fa-twitter-square"></i>
                            <span class="sr-only">Twitter</span>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#http://www.youtube.com/parlamentoandalucia">
                            <i class="fab fa-youtube"></i>
                            <span class="sr-only">Youtube</span>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="https://www.instagram.com/ParlamentoAnd">
                            <i class="fab fa-instagram"></i>
                            <span class="sr-only">Instagram</span>
                          </a>
                        </li>
                       
                      </ul>


                              </div>
                          </div>
                      </nav>
                  </div>
              </header>
             
    <section id="content">
      <h1 class="hide-accessible">${the_title}</h1>

      <#if selectable>
        <@liferay_util["include"] page=content_include />
        <#else>
          ${portletDisplay.recycle()}

          ${portletDisplay.setTitle(the_title)}

          <@liferay_theme["wrap-portlet"] page="portlet.ftl">
            <@liferay_util["include"] page=content_include />
            </@>
      </#if>
    </section>


    <footer id="footer" role="contentinfo">
      <div class="d-flex justify-content-between align-items-end">
        <div class="px-4 py-2">
          <img src="${images_folder}/logo-transparencia.svg" alt="" />
        </div>
        <div class="hercules px-4" style="margin-bottom: -.25rem;">
          <img src="${images_folder}/hercules.svg" alt="" width="100" />
        </div>
      </div>
      <div class="pa-footer-main bg-secondary text-light">
      <div>
      <#if has_navigation && is_setup_complete>
                      <#include "${full_templates_path}/navigation_footer.ftl" />
                    </#if>
      </div>
        <div class="row no-gutters py-3">
          <div class="col-sm-8">

           
              
            
            <nav class="navbar navbar-expand navbar-dark social pt-0">
             <ul class="navbar-nav">
                        <li class="nav-item">
                          <a class="nav-link" href="https://www.facebook.com/parlamentodeandalucia.es">
                            <i class="fab fa-facebook"></i>
                            <span class="sr-only">Facebook</span>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="https://twitter.com/parlamentoand">
                            <i class="fab fa-twitter-square"></i>
                            <span class="sr-only">Twitter</span>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#http://www.youtube.com/parlamentoandalucia">
                            <i class="fab fa-youtube"></i>
                            <span class="sr-only">Youtube</span>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="https://www.instagram.com/ParlamentoAnd">
                            <i class="fab fa-instagram"></i>
                            <span class="sr-only">Instagram</span>
                          </a>
                        </li>
                       
                      </ul>
            </nav>
          </div>
          <div class="col-sm-4 p-4 p-sm-0 pt-sm-4">
            <div class="row no-gutters">

              <div class="col-7 pr-3">
                <a href="#" class="navbar-brand w-100">
                  <img class="w-100" src="${images_folder}/logo-parlamento-blanco@2x.png" alt="Logo Parlamento de Andalucía">
                </a>
              </div>
              <div class="col-5"><i>c\ San Juan de Ribera,<br>s/n 41009 Sevilla.<br>Teléfono:
                  954592100</i></div>
            </div>
          </div>
        </div>
      </div>
      <div class="pa-footer-aux bg-primary  text-light">
       
      </div>
    </footer>

  </div>

  <@liferay_util["include"] page=body_bottom_include />

  <@liferay_util["include"] page=bottom_include />

<script src="${javascript_folder}/cookieconsent.min.js" data-cfasync="false"></script>


</body>

</html>