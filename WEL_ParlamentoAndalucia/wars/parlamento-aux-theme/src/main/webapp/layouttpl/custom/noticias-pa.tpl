
<div  id="main-content" role="main">
<div id="breadcrumb-pa">
	<div class="portlet-layout">	
		<div class="portlet-column portlet-column-only" id="column-1">
				$processor.processColumn("column-1", "portlet-column-content portlet-column-content-only")
			<!-- /#column-1 -->
		</div>
	</div><!-- /.portlet-layout -->
</div><!-- /#content -->
<div id="interior-pa" class="row">
	<div class="col-md-8 offset-md-2">
		<div class="portlet-layout">	
			<div class="portlet-column portlet-column-only" id="column-2">
					$processor.processColumn("column-2", "portlet-column-content portlet-column-content-only")
				<!-- /#column-2 -->
			</div>
		</div><!-- /.portlet-layout -->
	</div>
</div><!-- /#content -->
</div>