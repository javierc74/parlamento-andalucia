AUI().ready(

	/*
	This function gets loaded when all the HTML, not including the portlets, is
	loaded.
	*/

	function() {

		console.log('ss');

		if($(".cc-grower").length <= 0)
		window.cookieconsent.initialise({
			"palette": {
			  "popup": {
				"background": "#333333"
			  },
			  "button": {
				"background": "#F8F9FA"
			  }
			},
			"position": "top",
			"static": true,
			"content": {
			  "message": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eget ultrices nisi, et facilisis nunc.",
			  "dismiss": "Acepto",
			  "link": "Más información",
			  "href": "http://enlace"
			}
		  });
	}
);

Liferay.Portlet.ready(

	/*
	This function gets loaded after each and every portlet on the page.

	portletId: the current portlet's id
	node: the Alloy Node object of the current portlet
	*/

	function(portletId, node) {
	}
);

Liferay.on(
	'allPortletsReady',

	/*
	This function gets loaded when everything, including the portlets, is on
	the page.
	*/

	function() {
		$("#pa-search .lfr-ddm-field-group-inline button").removeClass("btn-light");
		$("#pa-search .lfr-ddm-field-group-inline button").addClass("btn-primary");
		$("#pa-search-movil .lfr-ddm-field-group-inline button").removeClass("btn-light");
		$("#pa-search-movil .lfr-ddm-field-group-inline button").addClass("btn-primary");
	}
);


